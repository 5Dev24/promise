﻿using System;
using System.Collections.Generic;

namespace uMod
{
    /// <summary>
    /// DynamicPool class
    /// Store pooled objects of generic type
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public sealed class PromisePool<T> where T : IPromiseNode
    {
        private readonly Queue<T> _objects = new Queue<T>();
        private readonly object _poolLock = new object();

        /// <summary>
        /// Create a dynamic pool object
        /// </summary>
        internal PromisePool()
        {
        }

        /// <summary>
        /// Get (or create) a new pooled object
        /// </summary>
        /// <returns></returns>
        public T Get()
        {
            lock (_poolLock)
            {
                if (_objects.Count == 0)
                {
                    return (T)Activator.CreateInstance(typeof(T));
                }
                else
                {
                    return _objects.Dequeue();
                }
            }
        }

        /// <summary>
        /// Free a pooled object back to the pool
        /// </summary>
        /// <param name="object"></param>
        public void Free(T @object)
        {
            lock (_poolLock)
            {
                if (@object == null)
                {
                    return;
                }

                _objects.Enqueue(@object);
            }
        }
    }
}
