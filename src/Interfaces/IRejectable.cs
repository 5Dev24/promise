﻿using System;

namespace uMod
{
    public interface IRejectable
    {
        void Reject(Exception exception);
    }
}
