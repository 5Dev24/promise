﻿using System;
using System.Collections.Generic;

namespace uMod
{
    public interface IPromise<TValue> : IPromiseNode
    {
        void Resolve(TValue value);

        void Reject(Exception exception);

        void ReportProgress(float progress);

        void Done(Action onResolved, Action<Exception> onRejected = null);

        void Done(Action<TValue> onResolved, Action<Exception> onRejected = null);

        bool Join(int timeout = -1);

        IPromise Fail(Action<Exception> onRejected);

        IPromise<TValue> Fail(Func<Exception, TValue> onRejected);

        IPromise<TConvertedValue> Then<TConvertedValue>(Func<TValue, IPromise<TConvertedValue>> onResolved, Func<Exception, IPromise<TConvertedValue>> onRejected = null, Action<float> onProgress = null);

        IPromise Then(Func<TValue, IPromise> onResolved, Action<Exception> onRejected = null, Action<float> onProgress = null);

        IPromise Then(Action onResolved, Action<Exception> onRejected = null, Action<float> onProgress = null);

        IPromise Then(Action<TValue> onResolved, Action<Exception> onRejected = null, Action<float> onProgress = null);

        IPromise Then(IPromise promise);

        IPromise Then(IPromise<TValue> promise);

        IPromise<TConvertedValue> Then<TConvertedValue>(Func<TValue, TConvertedValue> promise);

        IPromise<IEnumerable<TConvertedValue>> All<TConvertedValue>(Func<TValue, IEnumerable<IPromise<TConvertedValue>>> chain);

        IPromise All(Func<TValue, IEnumerable<IPromise>> chain);

        IPromise<TValue> Always(Action onComplete);

        IPromise Always(Func<IPromise> onComplete);

        IPromise<TConvertedValue> Always<TConvertedValue>(Func<IPromise<TConvertedValue>> onComplete);

        IPromise<TValue> Progress(Action<float> onProgress);
    }
}
