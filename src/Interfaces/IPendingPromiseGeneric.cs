﻿namespace uMod
{
    public interface IPendingPromise<in TValue> : IRejectable
    {
        void Resolve(TValue value);

        void ReportProgress(float progress);
    }
}
