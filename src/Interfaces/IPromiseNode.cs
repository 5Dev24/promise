﻿using System;

namespace uMod
{
    public interface IPromiseNode : IDisposable
    {
        PromiseState State { get; }
    }
}
