﻿using System;
using System.Collections.Generic;

namespace uMod
{
    public interface IPromise : IPromiseNode
    {
        void Resolve();

        void Reject(Exception exception);

        void ReportProgress(float progress);

        void Done(Action onResolved, Action<Exception> onRejected = null);

        bool Join(int timeout = -1);

        IPromise Fail(Action<Exception> onRejected);

        IPromise Then(Action onResolved);

        IPromise Then(IPromise promise);

        IPromise Then(Func<IPromise> onResolved, Action<Exception> onRejected = null, Action<float> onProgress = null);

        IPromise Then(Action onResolved, Action<Exception> onRejected = null, Action<float> onProgress = null);

        IPromise<TValue> Then<TValue>(Func<IPromise<TValue>> onResolved, Func<Exception, IPromise<TValue>> onRejected = null, Action<float> onProgress = null);

        IPromise All(Func<IEnumerable<IPromise>> chain);

        IPromise<IEnumerable<TValue>> All<TValue>(Func<IEnumerable<IPromise<TValue>>> chain);

        IPromise All(Func<IEnumerable<Func<IPromise>>> chain);

        IPromise Always(Action onComplete);

        IPromise Always(Func<IPromise> onComplete);

        IPromise<TValue> Always<TValue>(Func<IPromise<TValue>> onComplete);

        IPromise Progress(Action<float> onProgress);
    }
}
