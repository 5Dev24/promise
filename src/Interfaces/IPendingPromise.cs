﻿namespace uMod
{
    public interface IPendingPromise : IRejectable
    {
        void Resolve();

        void ReportProgress(float progess);
    }
}
