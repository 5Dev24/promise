﻿using System.Collections.Generic;
using System.Threading;
using System.Linq;
using System;

namespace uMod
{
    public class Promise<TValue> : IPromise<TValue>, IPendingPromise<TValue>
    {
        internal TValue ResolvedValue;
        internal Exception RejectionException;
        public PromiseState State { get; private set; }
        private IList<CallbackHandler<Exception>> _rejectionCallbacks;
        private IList<CallbackHandler<TValue>> _resolveCallbacks;
        private IList<CallbackHandler<float>> _progressCallbacks;
        private HashSet<IPromiseNode> _nodes;
        private EventWaitHandle _event;

        public Promise() : this(PromiseState.Pending) { }

        public Promise(Action<Action<TValue>, Action<Exception>> resolver) : this(PromiseState.Pending)
        {
            With(resolver);
        }

        /// <summary>
        /// Create a new promise in the given state.
        /// </summary>
        internal Promise(PromiseState state)
        {
            State = state;
        }

        [Obsolete("Use Promise.Resolved() or Promise.Rejected() instead.")]
        public Promise(PromiseState state, TValue resolvedValue = default, Exception rejectionException = null)
        {
            State = state;
            ResolvedValue = resolvedValue;
            RejectionException = rejectionException;
        }

        /// <summary>
        /// Returns a new Promise object that is resolved with the given value.
        /// </summary>
        public static Promise<TValue> Resolved(TValue resolvedValue = default)
        {
            var promise = new Promise<TValue>(PromiseState.Resolved)
            {
                ResolvedValue = resolvedValue
            };
            return promise;
        }

        /// <summary>
        /// Returns a new Promise object that is rejected with the given exception.
        /// </summary>
        public static Promise<TValue> Rejected(Exception rejectionException = null)
        {
            var promise = new Promise<TValue>(PromiseState.Rejected)
            {
                RejectionException = rejectionException
            };
            return promise;
        }

        internal IPromiseNode AddNode(IPromiseNode node)
        {
            if (node == null)
            {
                return null;
            }

            if (_nodes == null)
            {
                _nodes = new HashSet<IPromiseNode>();
            }

            _nodes.Add(node);

            return node;
        }

        public virtual void Dispose()
        {
            RejectionException = null;
            _rejectionCallbacks?.Clear();
            _resolveCallbacks?.Clear();
            _progressCallbacks?.Clear();
            _event?.Close();
            _rejectionCallbacks = null;
            _resolveCallbacks = null;
            _progressCallbacks = null;
            _event = null;

            if (_nodes != null)
            {
                foreach (IPromiseNode node in _nodes)
                {
                    node.Dispose();
                }
                _nodes.Clear();
            }
            State = PromiseState.Pending;
            _nodes = null;
            if (GetType() == typeof(Promise<TValue>))
            {
                Promises<TValue>.Pool.Free(this);
            }
            GC.SuppressFinalize(this);
        }

        internal IPromise<TValue> With(PromiseState state, TValue resolvedValue = default, Exception rejectionException = null)
        {
            State = state;
            ResolvedValue = resolvedValue;
            RejectionException = rejectionException;
            return this;
        }

        internal IPromise<TValue> With(Action<Action<TValue>, Action<Exception>> resolver)
        {
            if (State != PromiseState.Pending)
            {
                return this;
            }

            try
            {
                resolver(Resolve, Reject);
            }
            catch (Exception exception)
            {
                Reject(exception);
            }

            return this;
        }

        internal void AddCallback(CallbackHandler<Exception> rejectionCallback)
        {
            if (_rejectionCallbacks == null)
            {
                _rejectionCallbacks = new List<CallbackHandler<Exception>>();
            }

            _rejectionCallbacks.Add(rejectionCallback);
        }

        internal void AddCallback(CallbackHandler<float> progressCallback)
        {
            if (_progressCallbacks == null)
            {
                _progressCallbacks = new List<CallbackHandler<float>>();
            }

            _progressCallbacks.Add(progressCallback);
        }

        internal void AddCallback(CallbackHandler<TValue> resolveCallback)
        {
            if (_resolveCallbacks == null)
            {
                _resolveCallbacks = new List<CallbackHandler<TValue>>();
            }

            _resolveCallbacks.Add(resolveCallback);
        }

        private void ClearCallbacks()
        {
            _rejectionCallbacks = null;
            _resolveCallbacks = null;
            _progressCallbacks = null;
        }

        public virtual void Done(Action<TValue> onResolved, Action<Exception> onRejected = null)
        {
            PromiseResolver<TValue>.Invoke(this, onResolved, onRejected);
        }

        public virtual void Done(Action onResolved, Action<Exception> onRejected = null)
        {
            PromiseResolver<TValue>.Invoke(this, onResolved, onRejected);
        }

        /// <summary>
        /// Allows for the Promise to be joined, the signal of the event is
        /// only set when the Promise reaches a conclusion of Rejected or Resolved
        /// <para/>
        /// Returns if the Promise resolved before the timeout
        /// <para/>
        /// Returns true when a negative or no timeout is given
        /// </summary>
        public virtual bool Join(int timeout = -1)
        {
            if (State != PromiseState.Pending)
            {
                throw new PromiseStateException($"Cannot join a promise in state ({State}), must be ({PromiseState.Pending})");
            }

            if (_event == null)
            {
                _event = new EventWaitHandle(false, EventResetMode.ManualReset);
            }

            if (timeout < 0)
            {
                timeout = -1;
            }

            try
            {
                return _event.WaitOne(timeout);
            } catch (Exception e) when (e is ObjectDisposedException || e is AbandonedMutexException || e is InvalidOperationException)
            {
                return false;
            }
        }

        public virtual IPromise Fail(Action<Exception> onRejected)
        {
            return PromiseResolver<TValue>.Invoke(this, null, onRejected);
        }

        public virtual IPromise<TValue> Fail(Func<Exception, TValue> onRejected)
        {
            return PromiseResolver<TValue>.Invoke(this, null, onRejected);
        }

        public virtual IPromise<TConvertedValue> Then<TConvertedValue>(Func<TValue, IPromise<TConvertedValue>> onResolved, Func<Exception, IPromise<TConvertedValue>> onRejected = null, Action<float> onProgress = null)
        {
            return PromiseResolver<TValue>.Invoke(this, onResolved, onRejected, onProgress);
        }

        public virtual IPromise Then(Func<TValue, IPromise> onResolved, Action<Exception> onRejected = null, Action<float> onProgress = null)
        {
            return PromiseResolver<TValue>.Invoke(this, onResolved, onRejected, onProgress);
        }

        public virtual IPromise Then(Action<TValue> onResolved, Action<Exception> onRejected = null, Action<float> onProgress = null)
        {
            return PromiseResolver<TValue>.Invoke(this, onResolved, onRejected, onProgress);
        }

        public virtual IPromise Then(Action onResolved, Action<Exception> onRejected = null, Action<float> onProgress = null)
        {
            return PromiseResolver<TValue>.Invoke(this, onResolved, onRejected, onProgress);
        }

        public virtual IPromise Then(IPromise promise)
        {
            return Then(promise.Resolve, promise.Reject, promise.ReportProgress);
        }

        public IPromise Then(IPromise<TValue> promise)
        {
            return Then(promise.Resolve, promise.Reject, promise.ReportProgress);
        }

        public virtual IPromise<TConvertedValue> Then<TConvertedValue>(Func<TValue, TConvertedValue> promise)
        {
            return PromiseResolver<TValue>.Invoke(this,
                value =>
                    AddNode(Promises<TConvertedValue>.Pool.Get().With(PromiseState.Resolved, promise(value))) as
                        IPromise<TConvertedValue>);
        }

        public virtual IPromise<IEnumerable<TConvertedValue>> All<TConvertedValue>(Func<TValue, IEnumerable<IPromise<TConvertedValue>>> chain)
        {
            return PromiseResolver<TValue>.Invoke(this,
                value => PromiseResolver<TConvertedValue>.Invoke(chain(value)));
        }

        public virtual IPromise All(Func<TValue, IEnumerable<IPromise>> chain)
        {
            return PromiseResolver<TValue>.Invoke(this, value => PromiseResolver.Invoke(chain(value)));
        }

        /// <summary>
        /// Wait for all promises to be resolved, or for any to be rejected.
        ///
        /// If the returned promise resolves, it is resolved with an aggregating array of the values from the resolved promises, in the same order as defined in the parameters passed.
        /// If it rejects, it is rejected with the reason from the first passed promise that was rejected.
        /// </summary>
        public static IPromise<IEnumerable<TValue>> All(params IPromise<TValue>[] promises)
        {
            return All((IEnumerable<IPromise<TValue>>) promises);
        }

        /// <summary>
        /// Wait for all promises to be resolved, or for any to be rejected.
        ///
        /// If the returned promise resolves, it is resolved with an aggregating array of the values from the resolved promises, in the same order as defined in the enumerable of multiple promises.
        /// If it rejects, it is rejected with the reason from the first promise in the enumerable that was rejected.
        /// </summary>
        public static IPromise<IEnumerable<TValue>> All(IEnumerable<IPromise<TValue>> promises)
        {
            var promisesArray = promises.ToArray();
            if (promisesArray.Length == 0)
            {
                return Promise<IEnumerable<TValue>>.Resolved(Enumerable.Empty<TValue>());
            }

            var remainingCount = promisesArray.Length;
            var results = new TValue[promisesArray.Length];
            var resultPromise = new Promise<IEnumerable<TValue>>();

            for (var i = 0; i < promisesArray.Length; i++)
            {
                // Save index to local var so it can be used in callbacks.
                var index = i;

                promisesArray[index]
                    .Then(result =>
                    {
                        results[index] = result;

                        --remainingCount;
                        if (remainingCount <= 0 && resultPromise.State == PromiseState.Pending)
                        {
                            resultPromise.Resolve(results.AsEnumerable());
                        }
                    })
                    .Fail(rejectionException =>
                    {
                        if (resultPromise.State == PromiseState.Pending)
                        {
                            resultPromise.Reject(rejectionException);
                        }
                    });
            }

            return resultPromise;
        }

        /// <summary>
        /// Cast the given value as type TValue.
        /// </summary>
        private static TValue CastAsTValue<T>(T value) where T : TValue
        {
            return (TValue) value;
        }

        /// <summary>
        /// Wait for all promises to be resolved, or for any to be rejected.
        ///
        /// If the returned promise resolves, it is resolved with an aggregating array of the values from the resolved promises, in the same order as defined in the parameters passed.
        /// If it rejects, it is rejected with the reason from the first passed promise that was rejected.
        /// </summary>
        public static IPromise<IEnumerable<TValue>> All<T1, T2>(IPromise<T1> p1, IPromise<T2> p2) where T1 : TValue where T2 : TValue
        {
            return All(new IPromise<TValue>[] {
                p1.Then(CastAsTValue),
                p2.Then(CastAsTValue)
            });
        }

        /// <summary>
        /// Wait for all promises to be resolved, or for any to be rejected.
        ///
        /// If the returned promise resolves, it is resolved with an aggregating array of the values from the resolved promises, in the same order as defined in the parameters passed.
        /// If it rejects, it is rejected with the reason from the first passed promise that was rejected.
        /// </summary>
        public static IPromise<IEnumerable<TValue>> All<T1, T2, T3>(IPromise<T1> p1, IPromise<T2> p2, IPromise<T3> p3) where T1 : TValue where T2 : TValue where T3 : TValue
        {
            return All(new IPromise<TValue>[] {
                p1.Then(CastAsTValue),
                p2.Then(CastAsTValue),
                p3.Then(CastAsTValue)
            });
        }

        /// <summary>
        /// Wait for all promises to be resolved, or for any to be rejected.
        ///
        /// If the returned promise resolves, it is resolved with an aggregating array of the values from the resolved promises, in the same order as defined in the parameters passed.
        /// If it rejects, it is rejected with the reason from the first passed promise that was rejected.
        /// </summary>
        public static IPromise<IEnumerable<TValue>> All<T1, T2, T3, T4>(IPromise<T1> p1, IPromise<T2> p2, IPromise<T3> p3, IPromise<T4> p4) where T1 : TValue where T2 : TValue where T3 : TValue where T4 : TValue
        {
            return All(new IPromise<TValue>[] {
                p1.Then(CastAsTValue),
                p2.Then(CastAsTValue),
                p3.Then(CastAsTValue),
                p4.Then(CastAsTValue)
            });
        }

        /// <summary>
        /// Wait for all promises to be resolved, or for any to be rejected.
        ///
        /// If the returned promise resolves, it is resolved with an aggregating array of the values from the resolved promises, in the same order as defined in the parameters passed.
        /// If it rejects, it is rejected with the reason from the first passed promise that was rejected.
        /// </summary>
        public static IPromise<IEnumerable<TValue>> All<T1, T2, T3, T4, T5>(IPromise<T1> p1, IPromise<T2> p2, IPromise<T3> p3, IPromise<T4> p4, IPromise<T5> p5) where T1 : TValue where T2 : TValue where T3 : TValue where T4 : TValue where T5 : TValue
        {
            return All(new IPromise<TValue>[] {
                p1.Then(CastAsTValue),
                p2.Then(CastAsTValue),
                p3.Then(CastAsTValue),
                p4.Then(CastAsTValue),
                p5.Then(CastAsTValue)
            });
        }

        /// <summary>
        /// Wait for all promises to be resolved, or for any to be rejected.
        ///
        /// If the returned promise resolves, it is resolved with an aggregating array of the values from the resolved promises, in the same order as defined in the parameters passed.
        /// If it rejects, it is rejected with the reason from the first passed promise that was rejected.
        /// </summary>
        public static IPromise<IEnumerable<TValue>> All<T1, T2, T3, T4, T5, T6>(IPromise<T1> p1, IPromise<T2> p2, IPromise<T3> p3, IPromise<T4> p4, IPromise<T5> p5, IPromise<T6> p6) where T1 : TValue where T2 : TValue where T3 : TValue where T4 : TValue where T5 : TValue where T6 : TValue
        {
            return All(new IPromise<TValue>[] {
                p1.Then(CastAsTValue),
                p2.Then(CastAsTValue),
                p3.Then(CastAsTValue),
                p4.Then(CastAsTValue),
                p5.Then(CastAsTValue),
                p6.Then(CastAsTValue)
            });
        }

        /// <summary>
        /// Wait for all promises to be resolved, or for any to be rejected.
        ///
        /// If the returned promise resolves, it is resolved with an aggregating array of the values from the resolved promises, in the same order as defined in the parameters passed.
        /// If it rejects, it is rejected with the reason from the first passed promise that was rejected.
        /// </summary>
        public static IPromise<IEnumerable<TValue>> All<T1, T2, T3, T4, T5, T6, T7>(IPromise<T1> p1, IPromise<T2> p2, IPromise<T3> p3, IPromise<T4> p4, IPromise<T5> p5, IPromise<T6> p6, IPromise<T7> p7) where T1 : TValue where T2 : TValue where T3 : TValue where T4 : TValue where T5 : TValue where T6 : TValue where T7 : TValue
        {
            return All(new IPromise<TValue>[] {
                p1.Then(CastAsTValue),
                p2.Then(CastAsTValue),
                p3.Then(CastAsTValue),
                p4.Then(CastAsTValue),
                p5.Then(CastAsTValue),
                p6.Then(CastAsTValue),
                p7.Then(CastAsTValue)
            });
        }

        /// <summary>
        /// Wait for all promises to be resolved, or for any to be rejected.
        ///
        /// If the returned promise resolves, it is resolved with an aggregating array of the values from the resolved promises, in the same order as defined in the parameters passed.
        /// If it rejects, it is rejected with the reason from the first passed promise that was rejected.
        /// </summary>
        public static IPromise<IEnumerable<TValue>> All<T1, T2, T3, T4, T5, T6, T7, T8>(IPromise<T1> p1, IPromise<T2> p2, IPromise<T3> p3, IPromise<T4> p4, IPromise<T5> p5, IPromise<T6> p6, IPromise<T7> p7, IPromise<T8> p8) where T1 : TValue where T2 : TValue where T3 : TValue where T4 : TValue where T5 : TValue where T6 : TValue where T7 : TValue where T8 : TValue
        {
            return All(new IPromise<TValue>[] {
                p1.Then(CastAsTValue),
                p2.Then(CastAsTValue),
                p3.Then(CastAsTValue),
                p4.Then(CastAsTValue),
                p5.Then(CastAsTValue),
                p6.Then(CastAsTValue),
                p7.Then(CastAsTValue),
                p8.Then(CastAsTValue)
            });
        }

        public virtual IPromise<TValue> Always(Action onComplete)
        {
            return PromiseResolver<TValue>.Complete(this, onComplete);
        }

        public virtual IPromise Always(Func<IPromise> onComplete)
        {
            Promise promise = Promises.Pool.Get();

            Then(delegate (TValue value)
           {
               promise.Resolve();
           });
            Fail(delegate
            {
               promise.Resolve();
           });

            AddNode(promise);

            return promise.Then(onComplete);
        }

        public virtual IPromise<TConvertedValue> Always<TConvertedValue>(Func<IPromise<TConvertedValue>> onComplete)
        {
            Promise promise = Promises.Pool.Get();

            Then(delegate (TValue value)
           {
               promise.Resolve();
           });
            Fail(delegate
            {
               promise.Resolve();
           });

            return promise.Then(onComplete);
        }

        public virtual IPromise<TValue> Progress(Action<float> onProgress)
        {
            if (State == PromiseState.Pending && onProgress != null)
            {
                AddCallback(new CallbackHandler<float>(this, onProgress));
            }
            return this;
        }

        public virtual void ReportProgress(float progress)
        {
            if (State != PromiseState.Pending)
            {
                throw new PromiseStateException($"Cannot report progress on a promise in state ({State}), must be ({PromiseState.Pending})");
            }

            if (_progressCallbacks != null)
            {
                for (int i = 0; i < _progressCallbacks.Count; i++)
                {
                    _progressCallbacks[i].Invoke(progress);
                }
            }
        }

        public virtual void Resolve(TValue value)
        {
            if (State != PromiseState.Pending)
            {
                throw new PromiseStateException($"Cannot resolve a promise in state ({State}), must be ({PromiseState.Pending})");
            }

            ResolvedValue = value;
            State = PromiseState.Resolved;
            _event?.Set();

            if (_resolveCallbacks != null)
            {
                for (int i = 0; i < _resolveCallbacks.Count; i++)
                {
                    _resolveCallbacks[i].Invoke(value);
                }
            }

            ClearCallbacks();
        }

        public virtual void Reject(Exception exception)
        {
            if (State != PromiseState.Pending)
            {
                throw new PromiseStateException($"Cannot reject a promise in state ({State}), must be ({PromiseState.Pending})", exception);
            }

            RejectionException = exception;
            State = PromiseState.Rejected;
            _event?.Set();

            if (_rejectionCallbacks != null)
            {
                for (int i = 0; i < _rejectionCallbacks.Count; i++)
                {
                    _rejectionCallbacks[i].Invoke(exception);
                }
            }

            ClearCallbacks();
        }
    }
}
