﻿using System.Collections.Generic;
using System.Threading;
using System.Linq;
using System;

namespace uMod
{
    public class Promise : IPromise, IPendingPromise
    {
        internal Exception RejectionException;
        private IList<CallbackHandler<Exception>> _rejectionCallbacks;
        private IList<CallbackHandler> _resolveCallbacks;
        private IList<CallbackHandler<float>> _progressCallbacks;
        private HashSet<IPromiseNode> _nodes;
        private EventWaitHandle _event;

        public PromiseState State { get; private set; }

        public Promise() : this(PromiseState.Pending) {}

        public Promise(Action<Action, Action<Exception>> resolver) : this(PromiseState.Pending)
        {
            With(resolver);
        }

        /// <summary>
        /// Create a new promise in the given state.
        /// </summary>
        internal Promise(PromiseState state)
        {
            State = state;
        }

        [Obsolete("Use Promise.Resolved() or Promise.Rejected() instead.")]
        public Promise(PromiseState state, Exception rejectionException = null)
        {
            State = state;
            RejectionException = rejectionException;
        }

        /// <summary>
        /// Returns a new Promise object that is resolved with the given value.
        /// </summary>
        public static Promise Resolved()
        {
            return new Promise(PromiseState.Resolved);
        }

        /// <summary>
        /// Returns a new Promise object that is rejected with the given exception.
        /// </summary>
        public static Promise Rejected(Exception rejectionException = null)
        {
            var promise = new Promise(PromiseState.Rejected)
            {
                RejectionException = rejectionException
            };
            return promise;
        }

        internal IPromiseNode AddNode(IPromiseNode node)
        {
            if (node == null)
            {
                return null;
            }

            if (_nodes == null)
            {
                _nodes = new HashSet<IPromiseNode>();
            }

            _nodes.Add(node);

            return node;
        }

        public virtual void Dispose()
        {
            RejectionException = null;
            _rejectionCallbacks?.Clear();
            _resolveCallbacks?.Clear();
            _progressCallbacks?.Clear();
            _event?.Close();
            _rejectionCallbacks = null;
            _resolveCallbacks = null;
            _progressCallbacks = null;
            _event = null;

            if (_nodes != null)
            {
                foreach (IPromiseNode node in _nodes)
                {
                    node.Dispose();
                }
                _nodes.Clear();
            }
            State = PromiseState.Pending;
            _nodes = null;
            if (GetType() == typeof(Promise))
            {
                Promises.Pool.Free(this);
            }

            GC.SuppressFinalize(this);
        }

        internal IPromise With(PromiseState state, Exception rejectionException = null)
        {
            State = state;
            RejectionException = rejectionException;
            return this;
        }

        internal IPromise With(Action<Action, Action<Exception>> resolver)
        {
            if (State != PromiseState.Pending)
            {
                return this;
            }

            try
            {
                resolver(Resolve, Reject);
            }
            catch (Exception exception)
            {
                Reject(exception);
            }

            return this;
        }

        internal void AddCallback(CallbackHandler<Exception> rejectionCallback)
        {
            if (_rejectionCallbacks == null)
            {
                _rejectionCallbacks = new List<CallbackHandler<Exception>>();
            }

            _rejectionCallbacks.Add(rejectionCallback);
        }

        internal void AddCallback(CallbackHandler<float> progressCallback)
        {
            if (_progressCallbacks == null)
            {
                _progressCallbacks = new List<CallbackHandler<float>>();
            }

            _progressCallbacks.Add(progressCallback);
        }

        internal void AddCallback(CallbackHandler resolveCallback)
        {
            if (_resolveCallbacks == null)
            {
                _resolveCallbacks = new List<CallbackHandler>();
            }

            _resolveCallbacks.Add(resolveCallback);
        }

        private void ClearCallbacks()
        {
            _rejectionCallbacks = null;
            _progressCallbacks = null;
            _resolveCallbacks = null;
        }

        public virtual void Reject(Exception exception)
        {
            if (State != PromiseState.Pending)
            {
                throw new PromiseStateException($"Cannot reject a promise in state ({State}), must be ({PromiseState.Pending})", exception);
            }

            RejectionException = exception;
            State = PromiseState.Rejected;
            _event?.Set();

            if (_rejectionCallbacks != null)
            {
                for (int i = 0; i < _rejectionCallbacks.Count; i++)
                {
                    _rejectionCallbacks[i].Invoke(exception);
                }
            }

            ClearCallbacks();
        }

        public virtual void Resolve()
        {
            if (State != PromiseState.Pending)
            {
                throw new PromiseStateException($"Cannot resolve a promise in state ({State}), must be ({PromiseState.Pending})");
            }

            State = PromiseState.Resolved;
            _event?.Set();

            if (_resolveCallbacks != null)
            {
                for (int i = 0; i < _resolveCallbacks.Count; i++)
                {
                    _resolveCallbacks[i].Invoke();
                }
            }

            ClearCallbacks();
        }

        public virtual void ReportProgress(float progress)
        {
            if (State != PromiseState.Pending)
            {
                throw new PromiseStateException($"Cannot report progress on a promise in state ({State}), must be ({PromiseState.Pending})");
            }

            if (_progressCallbacks != null)
            {
                for (int i = 0; i < _progressCallbacks.Count; i++)
                {
                    _progressCallbacks[i].Invoke(progress);
                }
            }
        }

        public virtual void Done(Action onResolved, Action<Exception> onRejected = null)
        {
            PromiseResolver.Invoke(this, onResolved, onRejected);
        }

        /// <summary>
        /// Allows for the Promise to be joined, the signal of the event is
        /// only set when the Promise reaches a conclusion of Rejected or Resolved
        /// <para/>
        /// Returns if the Promise resolved before the timeout
        /// <para/>
        /// Returns true when a negative or no timeout is given
        /// </summary>
        public virtual bool Join(int timeout = -1)
        {
            if (State != PromiseState.Pending)
            {
                throw new PromiseStateException($"Cannot join a promise in state ({State}), must be ({PromiseState.Pending})");
            }

            if (_event == null)
            {
                _event = new EventWaitHandle(false, EventResetMode.ManualReset);
            }

            if (timeout < 0)
            {
                timeout = -1;
            }

            try
            {
                return _event.WaitOne(timeout);
            }
            catch (Exception e) when (e is ObjectDisposedException || e is AbandonedMutexException || e is InvalidOperationException)
            {
                return false;
            }
        }

        public virtual IPromise Then(Action onResolved)
        {
            return PromiseResolver.Invoke(this, onResolved);
        }

        public virtual IPromise Then(Action onResolved, Action<Exception> onRejected = null, Action<float> onProgress = null)
        {
            return PromiseResolver.Invoke(this, onResolved, onRejected, onProgress);
        }

        public virtual IPromise Then(Func<IPromise> onResolved, Action<Exception> onRejected = null, Action<float> onProgress = null)
        {
            return PromiseResolver.Invoke(this, onResolved, onRejected, onProgress);
        }

        public IPromise Then(IPromise promise)
        {
            return Then(promise.Resolve, promise.Reject, promise.ReportProgress);
        }

        public virtual IPromise Fail(Action<Exception> onRejected)
        {
            return PromiseResolver.Invoke(this, null, onRejected);
        }

        public virtual IPromise All(Func<IEnumerable<IPromise>> chain)
        {
            return PromiseResolver.Invoke(this, () => PromiseResolver.Invoke(chain.Invoke()));
        }

        public virtual IPromise All(Func<IEnumerable<Func<IPromise>>> chain)
        {
            return PromiseResolver.Invoke(this, () => PromiseResolver.Invoke(chain.Invoke()));
        }

        /// <summary>
        /// Wait for all promises to be resolved, or for any to be rejected.
        ///
        /// If the returned promise rejects, it is rejected with the reason from the first passed promise that was rejected.
        /// </summary>
        public static IPromise All(params IPromise[] promises)
        {
            return All((IEnumerable<IPromise>) promises);
        }


        /// <summary>
        /// Wait for all promises to be resolved, or for any to be rejected.
        ///
        /// If the returned promise rejects, it is rejected with the reason from the first promise in the enumerable that was rejected.
        /// </summary>
        public static IPromise All(IEnumerable<IPromise> promises)
        {
            var promisesArray = promises.ToArray();
            if (promisesArray.Length == 0)
            {
                return new Promise(PromiseState.Resolved);
            }

            var remainingCount = promisesArray.Length;
            var resultPromise = new Promise();

            for (var index = 0; index < promisesArray.Length; index++)
            {
                promisesArray[index]
                    .Then(() =>
                    {
                        --remainingCount;
                        if (remainingCount <= 0 && resultPromise.State == PromiseState.Pending)
                        {
                            resultPromise.Resolve();
                        }
                    })
                    .Fail(rejectionException =>
                    {
                        if (resultPromise.State == PromiseState.Pending)
                        {
                            resultPromise.Reject(rejectionException);
                        }
                    });
            }

            return resultPromise;
        }

        public virtual IPromise Always(Action onComplete)
        {
            return PromiseResolver.Complete(this, onComplete);
        }

        public virtual IPromise Always(Func<IPromise> onComplete)
        {
            Promise promise = Promises.Pool.Get();

            Then(promise.Resolve);
            Fail(delegate
            {
               promise.Resolve();
           });

            return promise.Then(onComplete);
        }

        public virtual IPromise<TValue> Always<TValue>(Func<IPromise<TValue>> onComplete)
        {
            Promise promise = Promises.Pool.Get();

            Then(promise.Resolve);
            Fail(delegate
            {
               promise.Resolve();
           });

            return promise.Then(onComplete);
        }

        public virtual IPromise Progress(Action<float> onProgress)
        {
            if (State == PromiseState.Pending && onProgress != null)
            {
                AddCallback(new CallbackHandler<float>(this, onProgress));
            }
            return this;
        }

        public virtual IPromise<TValue> Then<TValue>(Func<IPromise<TValue>> onResolved, Func<Exception, IPromise<TValue>> onRejected = null, Action<float> onProgress = null)
        {
            return PromiseResolver.Invoke(this, onResolved, onRejected, onProgress);
        }

        public virtual IPromise<IEnumerable<TValue>> All<TValue>(Func<IEnumerable<IPromise<TValue>>> chain)
        {
            return PromiseResolver.Invoke(this, () => PromiseResolver<TValue>.Invoke(chain()));
        }
    }
}
