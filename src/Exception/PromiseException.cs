﻿using System;

namespace uMod
{
    public class PromiseException : Exception
    {
        public PromiseException()
        {
        }

        public PromiseException(string message) : base(message)
        {
        }

        public PromiseException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
