﻿using System;

namespace uMod
{
    public class PromiseStateException : PromiseException
    {
        public PromiseStateException()
        {
        }

        public PromiseStateException(string message) : base(message)
        {
        }

        public PromiseStateException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
