﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace uMod
{
    internal class PromiseResolver<TValue>
    {
        public static IPromise Invoke(Promise<TValue> Promise, Func<TValue, IPromise> onResolved, Action<Exception> onRejected = null, Action<float> onProgress = null)
        {
            if (Promise.State == PromiseState.Resolved)
            {
                try
                {
                    return onResolved?.Invoke(Promise.ResolvedValue);
                }
                catch (Exception exception)
                {
                    return Promise.AddNode(Promises.Pool.Get().With(PromiseState.Rejected, exception)) as IPromise;
                }
            }

            Promise promise = Promises.Pool.Get();

            Action<Exception> rejectCallback;
            if (onRejected != null)
            {
                rejectCallback = delegate (Exception exception)
                {
                    onRejected.Invoke(exception);
                    promise.Reject(exception);
                };
            }
            else
            {
                rejectCallback = promise.Reject;
            }

            HandleResolvers(Promise, promise, delegate (TValue value)
           {
               if (onResolved != null)
               {
                   onResolved
                       .Invoke(value)
                       .Progress(promise.ReportProgress)
                       .Then(promise.Resolve, promise.Reject);
               }
               else
               {
                   promise.Resolve();
               }
            }, rejectCallback);
            if (onProgress != null)
            {
                Promise.AddCallback(new CallbackHandler<float>(Promise, onProgress));
            }

            return Promise.AddNode(promise) as IPromise;
        }

        public static IPromise<TValue> Invoke(Promise<TValue> Promise, Func<TValue, IPromise<TValue>> onResolved, Func<Exception, TValue> onRejected, Action<float> onProgress = null)
        {
            if (Promise.State == PromiseState.Resolved)
            {
                try
                {
                    onResolved?.Invoke(Promise.ResolvedValue);
                    return Promise;
                }
                catch (Exception exception)
                {
                    return Promise.AddNode(Promises<TValue>.Pool.Get().With(PromiseState.Rejected, default, exception)) as IPromise<TValue>;
                }
            }

            Promise<TValue> promise = Promises<TValue>.Pool.Get();

            Action<Exception> rejectCallback;
            if (onRejected != null)
            {
                rejectCallback = delegate (Exception exception)
                {
                    onRejected.Invoke(exception);
                    promise.Reject(exception);
                };
            }
            else
            {
                rejectCallback = promise.Reject;
            }

            HandleResolvers(Promise, promise, delegate (TValue value)
           {
               if (onResolved != null)
               {
                   onResolved
                       .Invoke(value)
                       .Progress(promise.ReportProgress)
                       .Then(promise.Resolve, promise.Reject);
               }
               else
               {
                   promise.Resolve(value);
               }
           }, rejectCallback);
            if (onProgress != null)
            {
                Promise.AddCallback(new CallbackHandler<float>(Promise, onProgress));
            }

            return Promise.AddNode(promise) as IPromise<TValue>;
        }

        public static IPromise Invoke(Promise<TValue> Promise, Action onResolved, Action<Exception> onRejected = null, Action<float> onProgress = null)
        {
            if (Promise.State == PromiseState.Resolved)
            {
                try
                {
                    onResolved?.Invoke();
                    return Promise.AddNode(Promises.Pool.Get().With(PromiseState.Resolved)) as IPromise;
                }
                catch (Exception exception)
                {
                    return Promise.AddNode(Promises.Pool.Get().With(PromiseState.Rejected, exception)) as IPromise;
                }
            }

            Promise promise = Promises.Pool.Get();

            Action<Exception> rejectCallback;
            if (onRejected != null)
            {
                rejectCallback = delegate (Exception exception)
                {
                    onRejected.Invoke(exception);
                    promise.Reject(exception);
                };
            }
            else
            {
                rejectCallback = promise.Reject;
            }

            HandleResolvers(Promise, promise, delegate
            {
               onResolved?.Invoke();
               promise.Resolve();
           }, rejectCallback);

            if (onProgress != null && Promise.State == PromiseState.Pending)
            {
                Promise.AddCallback(new CallbackHandler<float>(Promise, onProgress));
            }

            return Promise.AddNode(promise) as IPromise;
        }

        public static IPromise<TConvertedValue> Invoke<TConvertedValue>(Promise<TValue> Promise, Func<TValue, IPromise<TConvertedValue>> onResolved, Func<Exception, IPromise<TConvertedValue>> onRejected = null, Action<float> onProgress = null)
        {
            if (Promise.State == PromiseState.Resolved)
            {
                try
                {
                    return onResolved?.Invoke(Promise.ResolvedValue);
                }
                catch (Exception exception)
                {
                    return Promise.AddNode(Promises<TConvertedValue>.Pool.Get().With(PromiseState.Rejected, default, exception)) as IPromise<TConvertedValue>;
                }
            }

            Promise<TConvertedValue> promise = Promises<TConvertedValue>.Pool.Get();

            HandleResolvers(Promise, promise, delegate (TValue value)
           {
               onResolved?.Invoke(value)
                   .Progress(promise.ReportProgress)
                   .Then(promise.Resolve, promise.Reject);
           }, delegate (Exception exception)
           {
               if (onRejected == null)
               {
                   promise.Reject(exception);
                   return;
               }

               try
               {
                   onRejected.Invoke(exception).Then(promise.Resolve, promise.Reject);
               }
               catch (Exception callbackEx)
               {
                   promise.Reject(callbackEx);
               }
           });
            if (onProgress != null)
            {
                Promise.AddCallback(new CallbackHandler<float>(Promise, onProgress));
            }

            return Promise.AddNode(promise) as IPromise<TConvertedValue>;
        }

        public static IPromise Invoke(Promise<TValue> Promise, Action<TValue> onResolved, Action<Exception> onRejected = null, Action<float> onProgress = null)
        {
            if (Promise.State == PromiseState.Resolved)
            {
                try
                {
                    onResolved?.Invoke(Promise.ResolvedValue);
                    return Promise.AddNode(Promises.Pool.Get().With(PromiseState.Resolved)) as IPromise;
                }
                catch (Exception exception)
                {
                    return Promise.AddNode(Promises.Pool.Get().With(PromiseState.Rejected, exception)) as IPromise;
                }
            }

            Promise promise = Promises.Pool.Get();

            Action<Exception> rejectCallback;
            if (onRejected != null)
            {
                rejectCallback = delegate (Exception exception)
                {
                    onRejected.Invoke(exception);
                    promise.Reject(exception);
                };
            }
            else
            {
                rejectCallback = promise.Reject;
            }

            HandleResolvers(Promise, promise, delegate (TValue value)
           {
               onResolved?.Invoke(value);
               promise.Resolve();
           }, rejectCallback);

            if (onProgress != null && Promise.State == PromiseState.Pending)
            {
                Promise.AddCallback(new CallbackHandler<float>(Promise, onProgress));
            }

            return Promise.AddNode(promise) as IPromise;
        }

        public static IPromise<TValue> Complete(Promise<TValue> Promise, Action onComplete)
        {
            if (Promise.State == PromiseState.Resolved)
            {
                try
                {
                    onComplete?.Invoke();
                    return Promise;
                }
                catch (Exception exception)
                {
                    return Promise.AddNode(Promises<TValue>.Pool.Get().With(PromiseState.Rejected, default, exception)) as IPromise<TValue>;
                }
            }

            Promise<TValue> promise = Promises<TValue>.Pool.Get();

            Promise.Then(promise.Resolve);
            Promise.Fail(delegate (Exception exception)
           {
               try
               {
                   onComplete?.Invoke();
                   promise.Reject(exception);
               }
               catch (Exception promiseException)
               {
                   promise.Reject(promiseException);
               }
           });

            Promise.AddNode(promise);

            return promise.Then(delegate (TValue value)
           {
               onComplete?.Invoke();
               return value;
           });
        }

        public static IPromise<IEnumerable<TConvertedValue>> Invoke<TConvertedValue>(IEnumerable<IPromise<TConvertedValue>> chain)
        {
            return Invoke(chain.ToArray());
        }

        public static IPromise<IEnumerable<TConvertedValue>> Invoke<TConvertedValue>(IPromise<TConvertedValue>[] chain)
        {
            if (chain.Length == 0)
            {
                return Promises<IEnumerable<TConvertedValue>>.Pool.Get().With(PromiseState.Resolved, Enumerable.Empty<TConvertedValue>());
            }

            TConvertedValue[] results = new TConvertedValue[chain.Length];
            float[] progress = new float[chain.Length];
            Promise<IEnumerable<TConvertedValue>> promise = Promises<IEnumerable<TConvertedValue>>.Pool.Get();

            for (int i = 0; i <= chain.Length; i++)
            {
                int i1 = i;
                chain[i]
                    .Progress(delegate (float progressValue)
                   {
                       progress[i1] = progressValue;
                       if (promise.State == PromiseState.Pending)
                       {
                           promise.ReportProgress(progress.Average());
                       }
                   })
                    .Then(delegate (TConvertedValue value)
                   {
                       progress[i1] = 1f;
                       results[i1] = value;

                       if (i1 == chain.Length && promise.State == PromiseState.Pending)
                       {
                           promise.Resolve(results);
                       }
                   })
                    .Fail(delegate (Exception exception)
                   {
                       if (promise.State == PromiseState.Pending)
                       {
                           promise.Reject(exception);
                       }
                   });            }

            return promise;
        }

        private static void HandleResolvers(Promise<TValue> Promise, IRejectable promise, Action<TValue> resolveCallback, Action<Exception> rejectCallback)
        {
            switch (Promise.State)
            {
                case PromiseState.Resolved:
                    resolveCallback.Invoke(Promise.ResolvedValue);
                    break;

                case PromiseState.Rejected:
                    rejectCallback.Invoke(Promise.RejectionException);
                    break;

                default:
                    Promise.AddCallback(new CallbackHandler<TValue>(promise, resolveCallback));
                    Promise.AddCallback(new CallbackHandler<Exception>(promise, rejectCallback));
                    break;
            }
        }
    }
}
