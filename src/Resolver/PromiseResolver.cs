﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace uMod
{
    internal class PromiseResolver
    {
        public static IPromise<TValue> Invoke<TValue>(Promise Promise, Func<IPromise<TValue>> onResolved, Func<Exception, IPromise<TValue>> onRejected = null, Action<float> onProgress = null)
        {
            if (Promise.State == PromiseState.Resolved)
            {
                try
                {
                    return Promise.AddNode(onResolved?.Invoke()) as IPromise<TValue>;
                }
                catch (Exception ex)
                {
                    return Promise.AddNode(Promises<TValue>.Pool.Get().With(PromiseState.Rejected, default, ex)) as IPromise<TValue>;
                }
            }

            Promise<TValue> promise = Promises<TValue>.Pool.Get();

            HandleResolvers(Promise, promise, delegate
            {
               onResolved?
                   .Invoke()
                   .Progress(promise.ReportProgress)
                   .Then(promise.Resolve, promise.Reject);
           }, delegate (Exception exception)
           {
               if (onRejected == null)
               {
                   promise.Reject(exception);
                   return;
               }

               try
               {
                   onRejected.Invoke(exception).Then(promise.Resolve, promise.Reject);
               }
               catch (Exception callbackEx)
               {
                   promise.Reject(callbackEx);
               }
           });

            if (onProgress != null)
            {
                Promise.AddCallback(new CallbackHandler<float>(promise, onProgress));
            }

            return Promise.AddNode(promise) as IPromise<TValue>;
        }

        public static IPromise Invoke(Promise Promise, Func<IPromise> onResolved, Action<Exception> onRejected = null, Action<float> onProgress = null)
        {
            if (Promise.State == PromiseState.Resolved)
            {
                try
                {
                    onResolved?.Invoke();
                    return Promise;
                }
                catch (Exception exception)
                {
                    return Promise.AddNode(Promises.Pool.Get().With(PromiseState.Rejected, exception)) as IPromise;
                }
            }

            Promise promise = Promises.Pool.Get();

            Action resolveCallback;
            if (onResolved != null)
            {
                resolveCallback = delegate
                {
                    onResolved.Invoke();
                    promise.Resolve();
                };
            }
            else
            {
                resolveCallback = promise.Resolve;
            }

            Action<Exception> rejectCallback;

            if (onRejected != null)
            {
                rejectCallback = delegate (Exception exception)
                {
                    onRejected.Invoke(exception);
                    promise.Resolve();
                };
            }
            else
            {
                rejectCallback = promise.Reject;
            }

            HandleResolvers(Promise, promise, resolveCallback, rejectCallback);
            if (onProgress != null && Promise.State == PromiseState.Pending)
            {
                Promise.AddCallback(new CallbackHandler<float>(promise, onProgress));
            }

            return Promise.AddNode(promise) as IPromise;
        }

        public static IPromise Invoke(Promise Promise, Action onResolved, Action<Exception> onRejected = null, Action<float> onProgress = null)
        {
            if (Promise.State == PromiseState.Resolved)
            {
                try
                {
                    onResolved?.Invoke();
                    return Promise;
                }
                catch (Exception exception)
                {
                    return Promise.AddNode(Promises.Pool.Get().With(PromiseState.Rejected, exception)) as IPromise;
                }
            }

            Promise promise = Promises.Pool.Get();

            Action resolveCallback;
            if (onResolved != null)
            {
                resolveCallback = delegate
                {
                    onResolved.Invoke();
                    promise.Resolve();
                };
            }
            else
            {
                resolveCallback = promise.Resolve;
            }

            Action<Exception> rejectCallback;

            if (onResolved != null)
            {
                rejectCallback = delegate (Exception exception)
                {
                    if (onRejected != null)
                    {
                        onRejected.Invoke(exception);
                        promise.Resolve();
                        return;
                    }

                    promise.Reject(exception);
                };
            }
            else
            {
                rejectCallback = promise.Reject;
            }

            HandleResolvers(Promise, promise, resolveCallback, rejectCallback);
            if (onProgress != null && Promise.State == PromiseState.Pending)
            {
                Promise.AddCallback(new CallbackHandler<float>(promise, onProgress));
            }

            return Promise.AddNode(promise) as IPromise;
        }

        private static void HandleResolvers(Promise Promise, IRejectable promise, Action resolveCallback, Action<Exception> rejectCallback)
        {
            switch (Promise.State)
            {
                case PromiseState.Resolved:
                    resolveCallback.Invoke();
                    break;

                case PromiseState.Rejected:
                    rejectCallback.Invoke(Promise.RejectionException);
                    break;

                default:
                    Promise.AddCallback(new CallbackHandler(promise, resolveCallback));
                    Promise.AddCallback(new CallbackHandler<Exception>(promise, rejectCallback));
                    break;
            }
        }

        public static IPromise Invoke(IEnumerable<IPromise> chain)
        {
            return Invoke(chain.ToArray());
        }

        public static IPromise Invoke(IPromise[] chain)
        {
            if (chain.Length == 0)
            {
                return Promises.Pool.Get().With(PromiseState.Resolved);
            }

            Promise promise = Promises.Pool.Get();
            float[] progress = new float[chain.Length];

            for (int i = 0; i <= chain.Length; i++)
            {
                int i1 = i;
                chain[i]
                    .Progress(delegate (float progressValue)
                   {
                       progress[i1] = progressValue;
                       if (promise.State == PromiseState.Pending)
                       {
                           promise.ReportProgress(progress.Average());
                       }
                   })
                    .Then(delegate
                    {
                       progress[i1] = 1f;

                       if (i1 == chain.Length && promise.State == PromiseState.Pending)
                       {
                           promise.Resolve();
                       }
                   })
                    .Fail(delegate (Exception exception)
                   {
                       if (promise.State == PromiseState.Pending)
                       {
                           promise.Reject(exception);
                       }
                   });
            }

            return promise;
        }

        public static IPromise Invoke(IEnumerable<Func<IPromise>> chain)
        {
            return Invoke(chain.ToArray());
        }

        public static IPromise Invoke(Func<IPromise>[] chain)
        {
            Promise promise = Promises.Pool.Get();

            int count = 0;

            IPromise previousPromise = Promises.Pool.Get().With(PromiseState.Resolved);

            for (int i = 0; i < chain.Length; i++)
            {
                int currentSequence = count;
                count += 1;

                int count1 = count;
                int i1 = i;
                previousPromise
                    .Then(delegate
                    {
                       promise.ReportProgress(1f / count1 * currentSequence);
                       previousPromise = chain[i1].Invoke();
                   })
                    .Progress(delegate (float progress)
                   {
                       promise.ReportProgress(1f / count1 * (progress + currentSequence));
                   });
            }

            previousPromise
                .Then(promise.Resolve)
                .Fail(promise.Reject);

            return promise;
        }

        public static IPromise Complete(Promise Promise, Action onComplete)
        {
            if (Promise.State == PromiseState.Resolved)
            {
                try
                {
                    onComplete?.Invoke();
                    return Promise;
                }
                catch (Exception exception)
                {
                    return Promise.AddNode(Promises.Pool.Get().With(PromiseState.Rejected, exception)) as IPromise;
                }
            }

            Promise promise = Promises.Pool.Get();

            Promise.Then(promise.Resolve);
            Promise.Fail(e =>
           {
               try
               {
                   onComplete?.Invoke();
                   promise.Reject(e);
               }
               catch (Exception exception2)
               {
                   promise.Reject(exception2);
               }
           });

            Promise.AddNode(promise);

            return promise.Then(onComplete);
        }
    }
}
