﻿namespace uMod
{
    public enum PromiseState
    {
        Pending,
        Rejected,
        Resolved
    }
}
