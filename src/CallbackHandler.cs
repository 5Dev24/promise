﻿using System;

namespace uMod
{
    public struct CallbackHandler
    {
        public Action Callback;
        public IRejectable Rejectable;

        public CallbackHandler(IRejectable rejectable, Action callback)
        {
            Rejectable = rejectable;
            Callback = callback;
        }

        public void Invoke()
        {
            try
            {
                Callback();
            }
            catch (Exception exception)
            {
                Rejectable.Reject(exception);
            }
        }
    }
}
