﻿using System;

namespace uMod
{
    public struct CallbackHandler<T>
    {
        public Action<T> Callback;
        public IRejectable Rejectable;

        public CallbackHandler(IRejectable rejectable, Action<T> callback)
        {
            Rejectable = rejectable;
            Callback = callback;
        }

        public void Invoke(T value)
        {
            try
            {
                Callback(value);
            }
            catch (Exception exception)
            {
                Rejectable.Reject(exception);
            }
        }
    }
}
