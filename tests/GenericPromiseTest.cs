﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace uMod.Tests
{
    [TestClass]
    public class GenericPromiseTest
    {
        [TestMethod]
        public void ResolvePromise()
        {
            int promisedValue = 5;
            using Promise<int> promise = Promise<int>.Resolved(promisedValue);
            int completed = 0;
            promise.Then(delegate (int value)
            {
                Assert.AreEqual(promisedValue, value);
                completed++;
            });

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void ResolvePromiseStatic()
        {
            int promisedValue = 5;
            using Promise<int> promise = Promise<int>.Resolved(promisedValue);
            int completed = 0;
            promise.Then(delegate (int value)
            {
                Assert.AreEqual(promisedValue, value);
                completed++;
            });

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void RejectPromise()
        {
            Exception ex = new Exception();
            using Promise<int> promise = Promise<int>.Rejected(ex);
            int errors = 0;
            promise.Fail(delegate (Exception exception)
            {
                Assert.AreEqual(ex, exception);
                errors++;
            });

            Assert.AreEqual(1, errors);
        }

        [TestMethod]
        public void RejectPromiseStatic()
        {
            Exception ex = new Exception();
            using Promise<int> promise = Promise<int>.Rejected(ex);
            int errors = 0;
            promise.Fail(delegate (Exception exception)
            {
                Assert.AreEqual(ex, exception);
                errors++;
            });

            Assert.AreEqual(1, errors);
        }

        [TestMethod]
        [ExpectedException(typeof(PromiseStateException))]
        public void ExceptionAfterRejectReject()
        {
            using Promise<int> promise = new Promise<int>();
            promise.Reject(new Exception());
            promise.Reject(new Exception());
        }

        [TestMethod]
        [ExpectedException(typeof(PromiseStateException))]
        public void ExceptionAfterRejectResolve()
        {
            using Promise<int> promise = new Promise<int>();
            promise.Resolve(5);
            promise.Reject(new Exception());
        }

        [TestMethod]
        [ExpectedException(typeof(PromiseStateException))]
        public void ExceptionAfterResolveReject()
        {
            using Promise<int> promise = new Promise<int>();
            promise.Reject(new Exception());
            promise.Resolve(5);
        }

        [TestMethod]
        [ExpectedException(typeof(PromiseStateException))]
        public void ExceptionAfterResolveResolve()
        {
            using Promise<int> promise = new Promise<int>();
            promise.Resolve(5);
            promise.Resolve(5);
        }

        [TestMethod]
        public void ResolvePromiseThenPromiseCallback()
        {
            using Promise<int> promise = new Promise<int>(), nestedPromise = new Promise<int>();
            int completed = 0;
            int promisedValue = 15;

            nestedPromise.Done(delegate (int value)
            {
                Assert.AreEqual(promisedValue, value);
                completed++;
            });

            promise.Then(nestedPromise);

            promise.Then(delegate (int value)
            {
                Assert.AreEqual(promisedValue, value);
                completed++;
            });

            promise.Resolve(promisedValue);

            Assert.AreEqual(2, completed);
        }

        [TestMethod]
        public void ResolvePromiseThenCallback()
        {
            using Promise<int> promise = new Promise<int>();
            int completed = 0;
            int promisedValue = 15;

            promise.Then(delegate (int value)
            {
                Assert.AreEqual(promisedValue, value);
                completed++;
            });

            promise.Resolve(promisedValue);

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void ResolvePromiseThenCallbacks()
        {
            using Promise<int> promise = new Promise<int>();
            int completed = 0;

            promise.Then(delegate (int value)
            {
                Assert.AreEqual(1, completed++);
            });
            promise.Then(delegate (int value)
            {
                Assert.AreEqual(2, completed++);
            });

            promise.Resolve(1);

            Assert.AreEqual(2, completed);
        }

        [TestMethod]
        public void ResolvePromiseThenCallbackAfterResolve()
        {
            using Promise<int> promise = new Promise<int>();
            int completed = 0;
            int promisedValue = -10;

            promise.Resolve(promisedValue);

            promise.Then(delegate (int value)
            {
                Assert.AreEqual(promisedValue, value);
                completed++;
            });

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void RejectPromiseFailCallback()
        {
            using Promise<int> promise = new Promise<int>();
            Exception ex = new Exception();
            int completed = 0;
            promise.Fail(delegate (Exception exception)
            {
                Assert.AreEqual(ex, exception);
                completed++;
            });

            promise.Reject(ex);

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void RejectPromiseFailCallbacks()
        {
            using Promise<int> promise = new Promise<int>();
            Exception ex = new Exception();
            int completed = 0;

            promise.Fail(delegate (Exception exception)
            {
                Assert.AreEqual(ex, exception);
                Assert.AreEqual(1, completed++);
            });
            promise.Fail(delegate (Exception exception)
            {
                Assert.AreEqual(ex, exception);
                Assert.AreEqual(2, completed++);
            });

            promise.Reject(ex);

            Assert.AreEqual(2, completed);
        }

        [TestMethod]
        public void RejectPromiseThenCallbackAfterReject()
        {
            using Promise<int> promise = new Promise<int>();
            Exception ex = new Exception();
            promise.Reject(ex);

            int completed = 0;
            promise.Fail(delegate (Exception exception)
            {
                Assert.AreEqual(ex, exception);
                completed++;
            });

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void ResolvePromiseNoFailCallback()
        {
            using Promise<int> promise = new Promise<int>();
            promise.Fail(delegate (Exception exception)
            {
                throw new Exception();
            });

            promise.Resolve(5);
        }

        [TestMethod]
        public void RejectPromiseNoThenCallback()
        {
            using Promise<int> promise = new Promise<int>();
            promise.All(delegate (int value)
            {
                throw new Exception();
            });

            promise.Reject(new Exception());
        }

        [TestMethod]
        public void ResolvePromiseViaResolver()
        {
            using Promise<int> promise = new Promise<int>((resolve, reject) => resolve(5));
            int completed = 0;
            promise.Then(delegate (int value)
            {
                Assert.AreEqual(5, value);
                completed++;
            });

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void RejectPromiseViaResolver()
        {
            Exception ex = new Exception();
            using Promise<int> promise = new Promise<int>((resolve, reject) =>
             {
                 reject(ex);
             });
            int completed = 0;
            promise.Fail(delegate (Exception exception)
            {
                Assert.AreEqual(ex, exception);
                completed++;
            });

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void ExceptionRejectsPromiseViaResolver()
        {
            Exception ex = new Exception();
            using Promise<int> promise = new Promise<int>((resolve, reject) => throw ex);
            int completed = 0;
            promise.Fail(delegate (Exception exception)
            {
                Assert.AreEqual(ex, exception);
                completed++;
            });

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void ResolvePromiseDoneCallback()
        {
            using Promise<int> promise = new Promise<int>();
            int callback = 0;
            int expectedValue = 5;

            promise.Done(delegate (int value)
            {
                Assert.AreEqual(expectedValue, value);

                callback++;
            });

            promise.Resolve(expectedValue);

            Assert.AreEqual(1, callback);
        }

        [TestMethod]
        public void ResolvePromiseDoneWithoutArgCallback()
        {
            using Promise<int> promise = new Promise<int>();
            int callback = 0;

            promise.Done(delegate ()
            {
                callback++;
            });

            promise.Resolve(5);

            Assert.AreEqual(1, callback);
        }

        [TestMethod]
        public void ResolvePromiseDoneCallbacks()
        {
            using Promise<int> promise = new Promise<int>();
            int callback = 0;
            int errorCallback = 0;
            int expectedValue = 5;

            promise.Done(delegate (int value)
            {
                Assert.AreEqual(expectedValue, value);
                callback++;
            },
            delegate (Exception exception)
            {
                errorCallback++;
            });

            promise.Resolve(expectedValue);

            Assert.AreEqual(1, callback);
            Assert.AreEqual(0, errorCallback);
        }

        [TestMethod]
        public void ResolvePromiseExceptionDoneCallback()
        {
            using Promise<int> promise = new Promise<int>();
            int callback = 0;
            int errorCallback = 0;
            Exception expectedException = new Exception();

            promise
                .All(delegate (int value)
                {
                    throw expectedException;
                })
                .Done(delegate ()
                {
                    callback++;
                },
                delegate (Exception exception)
                {
                    Assert.AreEqual(expectedException, exception);

                    errorCallback++;
                });

            promise.Resolve(6);

            Assert.AreEqual(0, callback);
            Assert.AreEqual(1, errorCallback);
        }

        [TestMethod]
        public void ResolvePromiseAlwaysCallback()
        {
            using Promise<int> promise = new Promise<int>();
            int callback = 0;

            promise.Always(delegate ()
            {
                callback++;
            });

            promise.Resolve(0);

            Assert.AreEqual(1, callback);
        }

        [TestMethod]
        public void RejectPromiseAlwaysCallback()
        {
            using Promise<int> promise = new Promise<int>();
            int callback = 0;

            promise.Always(delegate ()
            {
                callback++;
            });

            promise.Reject(new Exception());

            Assert.AreEqual(1, callback);
        }

        [TestMethod]
        public void ResolvePromiseChainAlwaysCallback()
        {
            using Promise<int> promise = new Promise<int>();
            int callback = 0;
            int expectedValue = 5;

            promise
                .Always(delegate ()
                {
                    callback++;
                })
                .Then(delegate (int value)
                {
                    Assert.AreEqual(expectedValue, value);
                    callback++;
                });

            promise.Resolve(expectedValue);

            Assert.AreEqual(2, callback);
        }

        [TestMethod]
        public void RejectPromiseChainAlwaysCallback()
        {
            using Promise<int> promise = new Promise<int>();
            int callback = 0;

            promise
                .Always(delegate ()
                {
                    callback++;
                })
                .Fail(delegate (Exception exception)
                {
                    callback++;
                });

            promise.Reject(new Exception());

            Assert.AreEqual(2, callback);
        }

        [TestMethod]
        public void RejectPromiseChainAlwaysPromiseCallback()
        {
            using Promise<int> promise = new Promise<int>();
            int callback = 0;

            promise.Always(delegate ()
            {
                callback++;
                return Promise.Resolved();
            })
            .Then(delegate ()
            {
                callback++;
            });

            promise.Reject(new Exception());

            Assert.AreEqual(2, callback);
        }

        [TestMethod]
        public void RejectPromiseChainAlwaysGenericPromiseCallback()
        {
            using Promise<int> promise = new Promise<int>();
            int callback = 0;
            int expectedValue = 6;

            promise.Always(delegate ()
            {
                callback++;
                return Promise<int>.Resolved(expectedValue);
            })
            .Then(delegate (int value)
            {
                Assert.AreEqual(expectedValue, value);
                callback++;
            });

            promise.Reject(new Exception());

            Assert.AreEqual(2, callback);
        }

        [TestMethod]
        public void PromiseAllConstructors()
        {
            var promise1 = new Promise<int>();
            var promise2 = new Promise<int>();

            var all1 = Promise<int>.All(new Promise<int>[] { promise1, promise2 });
            var all2 = Promise<int>.All(promise1, promise2);

            promise1.Resolve(1);
            promise2.Resolve(2);

            Assert.AreEqual(PromiseState.Resolved, all1.State);
            Assert.AreEqual(PromiseState.Resolved, all2.State);
        }

        [TestMethod]
        public void PromiseAllResolvesWhenChildrenAreResolved()
        {
            var promise1 = new Promise<int>();
            var promise2 = new Promise<int>();

            var all = Promise<int>.All(promise1, promise2);

            var completed = 0;

            all.Then(v =>
            {
                ++completed;

                var values = v.ToArray();
                Assert.AreEqual(2, values.Length);
                Assert.AreEqual(1, values[0]);
                Assert.AreEqual(2, values[1]);
            });

            promise1.Resolve(1);
            promise2.Resolve(2);

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void PromiseAllRejectsWhenFirstChildPromiseRejects()
        {
            var promise1 = new Promise<int>();
            var promise2 = new Promise<int>();

            var all = Promise<int>.All(promise1, promise2);

            all.Then(v => throw new Exception());

            var errors = 0;
            all.Fail(e => ++errors);

            promise1.Reject(new Exception());
            promise2.Resolve(2);

            Assert.AreEqual(1, errors);
        }

        [TestMethod]
        public void PromiseAllRejectsWhenSecondChildPromiseRejects()
        {
            var promise1 = new Promise<int>();
            var promise2 = new Promise<int>();

            var all = Promise<int>.All(promise1, promise2);

            all.Then(v => throw new Exception());

            var errors = 0;
            all.Fail(e => ++errors);

            promise1.Resolve(2);
            promise2.Reject(new Exception());

            Assert.AreEqual(1, errors);
        }

        [TestMethod]
        public void PromiseAllResolvesIfThereAreNoPromises()
        {
            var all = Promise<int>.All();

            var completed = 0;
            all.Then(values =>
            {
                ++completed;
                Assert.AreEqual(Array.Empty<int>(), values);
            });

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void PromiseAllResolvesWhenAllPromisesAreAlreadyResolved()
        {
            var promise1 = Promise<int>.Resolved(1);
            var promise2 = Promise<int>.Resolved(2);

            var all = Promise<int>.All(promise1, promise2);

            var completed = 0;
            all.Then(v =>
            {
                ++completed;

                var values = v.ToArray();
                Assert.AreEqual(2, values.Length);
                Assert.AreEqual(1, values[0]);
                Assert.AreEqual(2, values[1]);
            });

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void PromiseAll2DifferentChildrenTypes()
        {
            var promise1 = new Promise<int>();
            var promise2 = new Promise<string>();

            var all = Promise<object>.All(promise1, promise2);

            var completed = 0;

            all.Then(v =>
            {
                ++completed;

                var values = v.ToArray();
                Assert.AreEqual(2, values.Length);
                Assert.AreEqual(1, (int) values[0]);
                Assert.AreEqual("foo", (string) values[1]);
            });

            promise1.Resolve(1);
            promise2.Resolve("foo");

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void PromiseAll3DifferentChildrenTypes()
        {
            var promise1 = new Promise<int>();
            var promise2 = new Promise<string>();
            var promise3 = new Promise<bool>();

            var all = Promise<object>.All(promise1, promise2, promise3);

            var completed = 0;

            all.Then(v =>
            {
                ++completed;

                var values = v.ToArray();
                Assert.AreEqual(3, values.Length);
                Assert.AreEqual(1, (int) values[0]);
                Assert.AreEqual("foo", (string) values[1]);
                Assert.AreEqual(true, (bool) values[2]);
            });

            promise1.Resolve(1);
            promise2.Resolve("foo");
            promise3.Resolve(true);

            Assert.AreEqual(1, completed);
        }


        [TestMethod]
        public void PromiseAll8DifferentChildrenTypes()
        {
            var promise1 = new Promise<int>();
            var promise2 = new Promise<string>();
            var promise3 = new Promise<bool>();
            var promise4 = new Promise<float>();
            var promise5 = new Promise<double>();
            var promise6 = new Promise<long>();
            var promise7 = new Promise<char>();
            var promise8 = new Promise<object>();

            var all = Promise<object>.All(promise1, promise2, promise3, promise4, promise5, promise6, promise7, promise8);

            var completed = 0;

            all.Then(v =>
            {
                ++completed;

                var values = v.ToArray();
                Assert.AreEqual(8, values.Length);

                int v0 = (int) values[0];
                string v1 = (string) values[1];
                bool v2 = (bool) values[2];
                float v3 = (float) values[3];
                double v4 = (double) values[4];
                long v5 = (long) values[5];
                char v6 = (char) values[6];
                object v7 = (object) values[7];
            });

            promise1.Resolve(1);
            promise2.Resolve("foo");
            promise3.Resolve(true);
            promise4.Resolve(2f);
            promise5.Resolve(3d);
            promise6.Resolve(4L);
            promise7.Resolve('A');
            promise8.Resolve(new object());

            Assert.AreEqual(1, completed);
        }
    }
}
