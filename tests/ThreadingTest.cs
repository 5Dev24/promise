﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Threading;

namespace uMod.Tests
{
    [TestClass]
    public class ThreadingTest
    {

        /* Due to the use of threads, all tests should have a timeout set */

        [TestMethod]
        [Timeout(500)]
        public void IndefiniteJoin()
        {
            List<int> eventList = new List<int>();
            using (EventWaitHandle joinWait = new EventWaitHandle(false, EventResetMode.ManualReset))
            using (Promise promise = new Promise())
            {
                Thread seperateThread = new Thread(new ThreadStart(() =>
                {
                    lock (eventList)
                        eventList.Add(1);

                    joinWait.Set();
                    promise.Join();
                }));

                lock (eventList)
                    eventList.Add(0);

                seperateThread.Start();
                joinWait.WaitOne();

                lock (eventList)
                    eventList.Add(2);

                promise.Resolve();
            }

            Assert.AreEqual("0 1 2", string.Join(" ", eventList));
        }

        [TestMethod]
        [Timeout(500)]
        public void DefiniteJoin()
        {
            List<int> eventList = new List<int>();
            using (EventWaitHandle joinWait = new EventWaitHandle(false, EventResetMode.ManualReset))
            using (Promise promise = new Promise())
            {
                Thread seperateThread = new Thread(new ThreadStart(() =>
                {
                    lock (eventList)
                        eventList.Add(1);

                    lock (eventList) // Lock will revent Promise from Resolving
                    {
                        joinWait.Set();
                        bool ret = promise.Join(200);
                        eventList.Add(ret ? 0 : 2);
                    }
                }));

                lock (eventList)
                    eventList.Add(0);

                seperateThread.Start();
                joinWait.WaitOne();

                lock (eventList)
                    eventList.Add(3);

                promise.Resolve();
            }

            Assert.AreEqual("0 1 2 3", string.Join(" ", eventList));
        }

        [TestMethod]
        [Timeout(500)]
        public void UnresolvedPromise()
        {
            using EventWaitHandle joinWait = new EventWaitHandle(false, EventResetMode.ManualReset);
            using Promise promise = new Promise();
            Thread seperateThread = new Thread(new ThreadStart(() =>
            {
                joinWait.Set();
                promise.Join();
            }));

            seperateThread.Start();
            joinWait.WaitOne();

            Assert.IsFalse(seperateThread.Join(200));

            promise.Resolve();
        }

        [TestMethod]
        [Timeout(500)]
        public void JoiningANonPendingPromise()
        {
            int amount = 0;
            try
            {
                using Promise promise = new Promise();
                promise.Resolve();
                promise.Join();
            }
            catch (PromiseStateException)
            {
                amount++;
            }
            try
            {
                using Promise promise = new Promise();
                promise.Reject(new PromiseException());
                promise.Join();
            }
            catch (PromiseStateException)
            {
                amount++;
            }

            Assert.AreEqual(2, amount);
        }

    }
}
