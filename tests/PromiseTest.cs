﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace uMod.Tests
{
    [TestClass]
    public class PromiseTest
    {
        [TestMethod]
        public void ResolvePromise()
        {
            using Promise promise = Promise.Resolved();
            int completed = 0;
            promise.Then(delegate ()
            {
                completed++;
            });

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void ResolvePromiseStatic()
        {
            using Promise promise = Promise.Resolved();
            int completed = 0;
            promise.Then(delegate ()
            {
                completed++;
            });

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void RejectPromise()
        {
            Exception ex = new Exception();
            using Promise promise = Promise.Rejected(ex);
            int errors = 0;
            promise.Fail(delegate (Exception exception)
            {
                Assert.AreEqual(ex, exception);
                errors++;
            });

            Assert.AreEqual(1, errors);
        }

        [TestMethod]
        public void RejectPromiseStatic()
        {
            Exception ex = new Exception();
            using Promise promise = Promise.Rejected(ex);
            int errors = 0;
            promise.Fail(delegate (Exception exception)
            {
                Assert.AreEqual(ex, exception);
                errors++;
            });

            Assert.AreEqual(1, errors);
        }

        [TestMethod]
        [ExpectedException(typeof(PromiseStateException))]
        public void ExceptionAfterRejectReject()
        {
            using Promise promise = new Promise();
            promise.Reject(new Exception());
            promise.Reject(new Exception());
        }

        [TestMethod]
        [ExpectedException(typeof(PromiseStateException))]
        public void ExceptionAfterRejectResolve()
        {
            using Promise promise = new Promise();
            promise.Resolve();
            promise.Reject(new Exception());
        }

        [TestMethod]
        [ExpectedException(typeof(PromiseStateException))]
        public void ExceptionAfterResolveReject()
        {
            using Promise promise = new Promise();
            promise.Reject(new Exception());
            promise.Resolve();
        }

        [TestMethod]
        [ExpectedException(typeof(PromiseStateException))]
        public void ExceptionAfterResolveResolve()
        {
            using Promise promise = new Promise();
            promise.Resolve();
            promise.Resolve();
        }

        [TestMethod]
        public void ResolvePromiseThenPromiseCallback()
        {
            using Promise promise = new Promise(), nestedPromise = new Promise();
            int completed = 0;

            nestedPromise.Done(delegate ()
            {
                completed++;
            });

            promise.Then(nestedPromise);

            promise.Then(delegate ()
            {
                completed++;
            });

            promise.Resolve();

            Assert.AreEqual(2, completed);
        }

        [TestMethod]
        public void ResolvePromiseThenCallback()
        {
            using Promise promise = new Promise();
            int completed = 0;

            promise.Then(delegate ()
            {
                completed++;
            });

            promise.Resolve();

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void ResolvePromiseThenCallbacks()
        {
            using Promise promise = new Promise();
            int completed = 0;

            promise.Then(delegate ()
            {
                Assert.AreEqual(1, completed++);
            });
            promise.Then(delegate ()
            {
                Assert.AreEqual(2, completed++);
            });

            promise.Resolve();

            Assert.AreEqual(2, completed);
        }

        [TestMethod]
        public void ResolvePromiseThenCallbackAfterResolve()
        {
            using Promise promise = new Promise();
            int completed = 0;

            promise.Resolve();

            promise.Then(delegate ()
            {
                completed++;
            });

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void RejectPromiseFailCallback()
        {
            using Promise promise = new Promise();
            Exception ex = new Exception();
            int completed = 0;
            promise.Fail(delegate (Exception exception)
            {
                Assert.AreEqual(ex, exception);
                completed++;
            });

            promise.Reject(ex);

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void RejectPromiseFailCallbacks()
        {
            using Promise promise = new Promise();
            Exception ex = new Exception();
            int completed = 0;

            promise.Fail(delegate (Exception exception)
            {
                Assert.AreEqual(ex, exception);
                Assert.AreEqual(1, completed++);
            });
            promise.Fail(delegate (Exception exception)
            {
                Assert.AreEqual(ex, exception);
                Assert.AreEqual(2, completed++);
            });

            promise.Reject(ex);

            Assert.AreEqual(2, completed);
        }

        [TestMethod]
        public void RejectPromiseThenCallbackAfterReject()
        {
            using Promise promise = new Promise();
            Exception ex = new Exception();
            promise.Reject(ex);

            int completed = 0;
            promise.Fail(delegate (Exception exception)
            {
                Assert.AreEqual(ex, exception);
                completed++;
            });

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void ResolvePromiseNoFailCallback()
        {
            using Promise promise = new Promise();
            promise.Fail(delegate (Exception exception)
            {
                throw new Exception();
            });

            promise.Resolve();
        }

        [TestMethod]
        public void RejectPromiseNoThenCallback()
        {
            using Promise promise = new Promise();
            promise.All((Func<IEnumerable<Func<IPromise>>>)delegate ()
            {
                throw new Exception();
            });

            promise.Reject(new Exception());
        }

        [TestMethod]
        public void ResolvePromiseViaResolver()
        {
            using Promise promise = new Promise((resolve, reject) => resolve());
            int completed = 0;
            promise.Then(delegate ()
            {
                completed++;
            });

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void RejectPromiseViaResolver()
        {
            Exception ex = new Exception();
            using Promise promise = new Promise((resolve, reject) => { reject(ex); });
            int completed = 0;
            promise.Fail(delegate (Exception exception)
            {
                Assert.AreEqual(ex, exception);
                completed++;
            });

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void ExceptionRejectsPromiseViaResolver()
        {
            Exception ex = new Exception();
            using Promise promise = new Promise((resolve, reject) => throw ex);
            int completed = 0;
            promise.Fail(delegate (Exception exception)
            {
                Assert.AreEqual(ex, exception);
                completed++;
            });

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void ResolvePromiseDoneCallback()
        {
            using Promise promise = new Promise();
            int callback = 0;

            promise.Done(delegate ()
            {
                callback++;
            });

            promise.Resolve();

            Assert.AreEqual(1, callback);
        }

        [TestMethod]
        public void ResolvePromiseDoneCallbacks()
        {
            using Promise promise = new Promise();
            int callback = 0;
            int errorCallback = 0;

            promise.Done(delegate ()
            {
                callback++;
            },
            delegate (Exception exception)
            {
                errorCallback++;
            });

            promise.Resolve();

            Assert.AreEqual(1, callback);
            Assert.AreEqual(0, errorCallback);
        }

        [TestMethod]
        public void ResolvePromiseExceptionDoneCallback()
        {
            using Promise promise = new Promise();
            int callback = 0;
            int errorCallback = 0;
            Exception expectedException = new Exception();

            promise
                .All((Func<IEnumerable<Func<IPromise>>>)delegate ()
                {
                    throw expectedException;
                })
                .Done(delegate ()
                {
                    callback++;
                },
                delegate (Exception exception)
                {
                    Assert.AreEqual(expectedException, exception);

                    errorCallback++;
                });

            promise.Resolve();

            Assert.AreEqual(0, callback);
            Assert.AreEqual(1, errorCallback);
        }

        [TestMethod]
        public void ResolvePromiseAlwaysCallback()
        {
            using Promise promise = new Promise();
            int callback = 0;

            promise.Always(delegate ()
            {
                callback++;
            });

            promise.Resolve();

            Assert.AreEqual(1, callback);
        }

        [TestMethod]
        public void RejectPromiseAlwaysCallback()
        {
            using Promise promise = new Promise();
            int callback = 0;

            promise.Always(delegate ()
            {
                callback++;
            });

            promise.Reject(new Exception());

            Assert.AreEqual(1, callback);
        }

        [TestMethod]
        public void ResolvePromiseChainAlwaysCallback()
        {
            using Promise promise = new Promise();
            int callback = 0;

            promise
                .Always(delegate ()
                {
                    callback++;
                })
                .Then(delegate ()
                {
                    callback++;
                });

            promise.Resolve();

            Assert.AreEqual(2, callback);
        }

        [TestMethod]
        public void RejectPromiseChainAlwaysCallback()
        {
            using Promise promise = new Promise();
            int callback = 0;

            promise
                .Always(delegate ()
                {
                    callback++;
                })
                .Fail(delegate (Exception exception)
                {
                    callback++;
                });

            promise.Reject(new Exception());

            Assert.AreEqual(2, callback);
        }

        [TestMethod]
        public void RejectPromiseChainAlwaysPromiseCallback()
        {
            using Promise promise = new Promise();
            int callback = 0;

            promise.Always(delegate ()
            {
                callback++;
                return Promise.Resolved();
            })
            .Then(delegate ()
            {
                callback++;
            });

            promise.Reject(new Exception());

            Assert.AreEqual(2, callback);
        }

        [TestMethod]
        public void RejectPromiseChainAlwaysGenericPromiseCallback()
        {
            using Promise promise = new Promise();
            int callback = 0;
            int expectedValue = 6;

            promise.Always(delegate ()
            {
                callback++;
                return Promise<int>.Resolved(expectedValue);
            })
            .Then(delegate (int value)
            {
                Assert.AreEqual(expectedValue, value);
                callback++;
            });

            promise.Reject(new Exception());

            Assert.AreEqual(2, callback);
        }

        [TestMethod]
        public void PromiseAllConstructors()
        {
            var promise1 = new Promise();
            var promise2 = new Promise();

            var all1 = Promise.All(new Promise[] { promise1, promise2 });
            var all2 = Promise.All(promise1, promise2);

            promise1.Resolve();
            promise2.Resolve();

            Assert.AreEqual(PromiseState.Resolved, all1.State);
            Assert.AreEqual(PromiseState.Resolved, all2.State);
        }

        [TestMethod]
        public void PromiseAllResolvesWhenChildrenAreResolved()
        {
            var promise1 = new Promise();
            var promise2 = new Promise();

            var all = Promise.All(promise1, promise2);

            var completed = 0;

            all.Then(() =>
            {
                ++completed;
            });

            promise1.Resolve();
            promise2.Resolve();

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void PromiseAllRejectsWhenFirstChildPromiseRejects()
        {
            var promise1 = new Promise();
            var promise2 = new Promise();

            var all = Promise.All(promise1, promise2);

            all.Then(() => throw new Exception());

            var errors = 0;
            all.Fail(e => ++errors);

            promise1.Reject(new Exception());
            promise2.Resolve();

            Assert.AreEqual(1, errors);
        }

        [TestMethod]
        public void PromiseAllRejectsWhenSecondChildPromiseRejects()
        {
            var promise1 = new Promise();
            var promise2 = new Promise();

            var all = Promise.All(promise1, promise2);

            all.Then(() => throw new Exception());

            var errors = 0;
            all.Fail(e => ++errors);

            promise1.Resolve();
            promise2.Reject(new Exception());

            Assert.AreEqual(1, errors);
        }

        [TestMethod]
        public void PromiseAllResolvesIfThereAreNoPromises()
        {
            var all = Promise.All();

            var completed = 0;
            all.Then(() =>
            {
                ++completed;
            });

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void PromiseAllResolvesWhenAllPromisesAreAlreadyResolved()
        {
            var promise1 = Promise.Resolved();
            var promise2 = Promise.Resolved();

            var all = Promise.All(promise1, promise2);

            var completed = 0;
            all.Then(() =>
            {
                ++completed;
            });

            Assert.AreEqual(1, completed);
        }
    }
}
