# Promise

A+ promise implementation heavily inspired by https://github.com/Real-Serious-Games/C-Sharp-Promise/

Why is this different?

Some features that we would not use are removed.

Uses our deployment workflow, MSTest, GitLab CI.

Optimized for code-reuse and performance.

Adding pooling of promise objects.
